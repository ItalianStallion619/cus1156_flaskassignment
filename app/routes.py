from flask import render_template, flash, redirect, url_for, request
from app import app, db
from sqlalchemy import *
from app.forms import LoginForm, RegistrationForm, PatientForm, SearchForm, TreatmentForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Patient
from werkzeug.urls import url_parse

@app.route('/')
@app.route('/index')
@login_required
def index():
    return render_template('index.html', title='Home Page')

    
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/add_patient', methods=['GET', 'POST'])
def add_patient():
    if current_user.is_authenticated: #If user is logged in, then grant access to creating new patients.
        form = PatientForm()
    if form.validate_on_submit():
        patient = Patient(first_name=form.first_name.data, last_name=form.last_name.data, ssn=form.ssn.data, mobile_phone=form.mobile_phone.data, diagnosis=form.diagnosis.data, treatment_notes=form.treatment_notes.data, insurance=form.insurance.data)
        db.session.add(patient)
        db.session.commit()
        flash('Patient added.')
        return redirect(url_for('confirm_patient'))
    return render_template('add_patient.html', title='Add Patient', form=form)

@app.route('/confirm')
def confirm_patient():
    patient = Patient.query.all()
    last_patient = patient[-1]
    return render_template('confirm_patient.html', title="Confirmation Page", last_patient=patient[-1])
    
@app.route('/revise', methods=['GET', 'POST'])
def revise():
    patient = Patient.query.all()
    last_patient = patient[-1]
    form = PatientForm(obj=last_patient)
    db.session.delete(last_patient)
    if form.validate_on_submit():
        new_patient = Patient(first_name=form.first_name.data, last_name=form.last_name.data, ssn=form.ssn.data, mobile_phone=form.mobile_phone.data, diagnosis=form.diagnosis.data, treatment_notes=form.treatment_notes.data, insurance=form.insurance.data) #Info that was typed in
        db.session.add(new_patient)
        db.session.commit()
        flash("Validation Complete.")
        return redirect(url_for('confirm_patient'))
    return render_template('revise.html', title='Revise Patient List', form=form, last_patient=patient[-1])

@app.route('/patient_list', methods=['GET', 'POST'])
def patient_list():
    patient_list = Patient.query.all()
    return render_template('patient_list.html', title='Patient List', patient_list=Patient.query.all())

@app.route('/search', methods=['GET','POST'])
def search():
    search_form = SearchForm()
    if search_form.validate_on_submit():
        search_first_name = search_form.first_name.data
        search_last_name = search_form.last_name.data
        found_first_name = ""
        found_last_name = ""
        found_treatment_notes = ""
        if not search_first_name == "" and not search_last_name == "":
            if db.session.query(Patient).filter_by(first_name=search_first_name).first() and db.session.query(Patient).filter_by(last_name=search_last_name).first() != None:
                found_first_name = db.session.query(Patient).filter_by(first_name=search_first_name).first().first_name
                found_last_name = db.session.query(Patient).filter_by(last_name=search_last_name).first().last_name
                found_treatment_notes = db.session.query(Patient).filter_by().first().treatment_notes
                return redirect(url_for('patient_lookup',first_name=found_first_name, last_name=found_last_name, treatment_notes=found_treatment_notes))
            else:
                flash('Name not found.')
        elif not search_last_name == "":    
            if db.session.query(Patient).filter_by(last_name=search_last_name).first() != None:
                found_last_name = db.session.query(Patient).filter_by(last_name=search_last_name).first().last_name
                found_treatment_notes = db.session.query(Patient).filter_by().first().treatment_notes 
                return redirect(url_for('patient_lookup',first_name="", last_name=found_last_name, treatment_notes=found_treatment_notes))
            else:
                flash('Name not found.')
        elif not search_first_name == "":
            if db.session.query(Patient).filter_by(first_name=search_first_name).first() != None:
                found_first_name == db.session.query(Patient).filter_by(first_name=search_first_name).first().first_name
                found_treatment_notes = db.session.query(Patient).filter_by().first().treatment_notes 
                return redirect(url_for('patient_lookup',first_name=found_first_name, last_name="", treatment_notes=found_treatment_notes))
            else:
                flash('Name not found.')
        else:
            return render_template('search.html', title="Search",search_form=search_form, patient_list = Patient.query.all())
    return render_template('search.html', title="Search",search_form=search_form, patient_list = Patient.query.all())

@app.route('/patient_lookup/<first_name>/<last_name>/<treatment_notes>', methods=['GET', 'POST'])
@app.route('/patient_lookup/<first_name>//<treatment_notes>', methods=['GET', 'POST'])
@app.route('/patient_lookup/<last_name>//<treatment_notes>', methods=['GET', 'POST'])
def patient_lookup(first_name="", last_name="", treatment_notes=""):
    return render_template('patient_lookup.html',patient_first_name=first_name, patient_last_name=last_name, patient_treatment_notes=treatment_notes)

@app.route('/add_treatment/<first_name>/<last_name>/<treatment_notes>/', methods=['GET', 'POST'])
def add_treatment(first_name="", last_name="", treatment_notes=""):
    treatment_form = TreatmentForm()
    if treatment_form.validate_on_submit():
        treatment_notes
        flash('Treatment added!')
        return render_template('add_treatment.html', patient_treatment_form=treatment_form, patient_first_name=first_name, patient_last_name=last_name, patient_treatment_notes=treatment_notes)