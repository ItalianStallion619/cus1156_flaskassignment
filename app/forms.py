from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, FormField
from wtforms.fields.html5 import TelField
from wtforms.validators import DataRequired, Email, EqualTo, ValidationError
from app.models import User

class LoginForm(FlaskForm):
    username = StringField('Username', validators = [DataRequired()])
    password = PasswordField('Password', validators = [DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class PatientForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    ssn = StringField('Social Security Number', validators=[DataRequired()])
    mobile_phone = TelField('Mobile Number')
    diagnosis = StringField('Diagnosis', validators=[DataRequired()])
    treatment_notes = StringField('Treatment Notes')
    insurance = StringField('Insurance Provider', validators=[DataRequired()])
    submit = SubmitField('Add Patient')

class SearchForm(FlaskForm):
    first_name = StringField()
    last_name = StringField()
    submit = SubmitField('Search Patient')

class TreatmentForm(FlaskForm):
    first_name = StringField('First Name')
    last_name = StringField('Last Name')
    treatment_notes = StringField('Treatment Notes', validators=[DataRequired()])
    submit = SubmitField('Add Notes')